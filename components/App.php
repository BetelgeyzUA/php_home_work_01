<?php
namespace components;

class App{
	public function __construct(){
		// Форма для ввода GET запроса
		print ('<form method="GET" action="/"><input type="text" name="search" size="50"> <input type=submit value="Поиск"></form>');
		// Обьяевление переменных
		$str_get_temp = ''; // Переменая для временого храненения строки 
		$str_get_length = 0; // Переменая для подсчета длины строки
		$str_get_word_quality = 0; // Переменая для хранения количества слов в строке
		$str_get_space_quality = 0; // Переменая для хранения количества пробелов в строке
		$str_get_arr = []; // Массив хранящий слова из которых состоит строка
		$str_get_arr_letter_temp = []; // Массив хранящий букв из которых состоит строка
		$str_get_arr_letter = []; // Массив хранящий букв из которых состоит строка
		$str_get_word_max = ''; // Переменая для хранения самого длинного слова в строке
		$str_get_word_min = ''; // Переменая для хранения самого короткого слова в строке
		$str_get_word_avarage = 0; // Переменая для хранения среднюю длину слов в строке
  		// Парсинг строки с GET запроса
  		foreach ($_GET as $key => $value) {
  			$str_get_temp = $value;
  		}
  		// Начало обработки строки
		$str_get_length = iconv_strlen($str_get_temp,'UTF-8'); // Получение длины строки
		$str_get_space_quality = substr_count($str_get_temp, ' '); // Получение количества пробелов в строке
		$str_get_temp = trim($str_get_temp); // Удаление лишних пробелов и спец. символов в начале и конце строки
		$str_get_temp = preg_replace('/\s+/',' ',$str_get_temp); // Обьеденение с помошью RegEx много идущих подряд пробелов в один для правильного подсчета количества слов в строке
		$str_get_arr = explode(" ", $str_get_temp); // Разбитие строки на касив слов
		$str_get_word_quality = count($str_get_arr); // Получение  количества слов в строке
		$str_get_word_max = $str_get_arr[0]; // Присвоение максимально длинного слова в строке первому слову
		$str_get_word_min = $str_get_arr[0];// Присвоение минимально короткого слова в строке первому слову
		$str_get_temp = ""; // Обнуление временой строки
		// Поиск максимального и минималльного слова в строке
		for ($i = 0; $i < $str_get_word_quality ; $i++) {
  			if (iconv_strlen($str_get_word_max,'UTF-8') < iconv_strlen($str_get_arr[$i],'UTF-8')) {
  				//Если слово максимальное записываем его в переменую $str_get_word_max
  				 $str_get_word_max	= $str_get_arr[$i]; 
  			}
  			if (iconv_strlen($str_get_word_min,'UTF-8') > iconv_strlen($str_get_arr[$i],'UTF-8')) {
  				//Если слово минимальное записываем его в переменую $str_get_word_max
  				 $str_get_word_min	= $str_get_arr[$i]; 
  			}
  			// Сееденяем слова в одно слово для подсчета средний длины слова в строке
  			$str_get_temp = $str_get_temp.$str_get_arr[$i]; 

  		}
  		// Подсчей средней длины слова с строке
  		$str_get_word_avarage = iconv_strlen($str_get_temp, 'UTF-8') / $str_get_word_quality;
		$str_get_arr_letter_temp = str_split($str_get_temp); // Разбитие строки на мамсив букв
	    // Подсчен вхождение букв в слово
		foreach ($str_get_arr_letter_temp as $key => $value) {
			if (!array_key_exists($value, $str_get_arr_letter)) {
			 	$str_get_arr_letter[$value] = 1;
			 } else {
			 	$str_get_arr_letter[$value]++;
			 }
		}

    	// Вывод данных на экран
		if (isset($_GET['search'])) {
			print("Строка запроса: ".$_GET['search']."<br><hr>");
			print("Длина строки запроса: $str_get_length<br><hr>");
			print("Количество пробелов: $str_get_space_quality<br><hr>");
			print("Количество слов в строке: $str_get_word_quality<br><hr>");
			print("Самое длиное слово в масиве: $str_get_word_max<br><hr>");
			print("Самое короское слово в масиве: $str_get_word_min<br><hr>");
			print("Cредняя длина слова в масиве: $str_get_word_avarage<br><hr>");
			foreach ($str_get_arr_letter as $key => $value) {
				print("Бувка: $key = $value<br>");
			}
		} else {
			print("Запрос не веден");
		}

		// Калькулятор
		print("<br><br>Калькулятор<br><br>");
		print ('<form method="GET" action="/"><input type="text" name="value_a" size="10"> <input type="text" name="value_b" size="10"> <input type=submit value="=" name="sum"><br><input type="radio" name="operator" value="+" checked> + <input type="radio" name="operator" value="-"> - <input type="radio" name="operator" value="*"> * <input type="radio" name="operator" value="/"> /</form>');
		//
		$value_a = isset($_GET['value_a']) ? $_GET['value_a'] : 0;
		$value_b = isset($_GET['value_b']) ? $_GET['value_b'] : 0;
		$operator = isset($_GET['operator']) ? $_GET['operator'] : "+";
		$sum = 0;
		if (isset($_GET['sum'])) {
			switch ($operator) {
				case '+':
					$sum = $value_a + $value_b;
					break;	
			}
			switch ($operator) {
				case '-':
					$sum = $value_a - $value_b;
					break;	
			}
			switch ($operator) {
				case '*':
					$sum = $value_a * $value_b;
					break;	
			}
			switch ($operator) {
				case '/':
					if ($value_a == 0 || $value_b == 0) {
						break;
					} else {
						$sum = $value_a / $value_b;
					}			
					break;	
			}
		}
	// Проверка которая проверяет являются ли введимые данные числом если нет выводит информационое сообщение
	if (!is_numeric($value_a) || !is_numeric($value_b)) {		
		print('Это калькулятор допускается ввод только целых чисел или дробных через точку "."'); 
	}
	// Проверка которая проверяет не происходит ли деление на 0
	elseif (($value_a == 0 || $value_b == 0) && $operator == "/" ) {
	    print('Деление на 0');
	}
	// Если все введено правильно выводим результат
	else {
			if(isset($_GET['sum'])) print("Результат $value_a $operator $value_b = $sum");
	}
	print("<br>");

	}
	public function run(){
		return 0;
	}
}
?>
