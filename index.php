<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
/**
 * 
 * Методы для подключения файлов 
 * @read http://php.net/manual/ru/function.require.php
 */
require(__DIR__ . '/bootstrap.php');
use components\App;

$application = new App();
exit($application->run());
?>
